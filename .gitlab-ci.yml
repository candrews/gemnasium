variables:
  DS_EXCLUDED_PATHS: "spec, test, tests, tmp, fixtures, testdata"

stages:
  - pre-build
  - image
  - test
  - tag

include:
  - template: Container-Scanning.gitlab-ci.yml
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/go.yml'
  - project: 'gitlab-org/security-products/ci-templates'
    ref: 'master'
    file: '/includes-dev/upsert-git-tag.yml'

# enable Merge Request pipelines (https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)
workflow:
  rules:
    # Workaround to get Secure templates to run in a Merge Request pipeline, as explained
    # here: https://gitlab.com/gitlab-org/gitlab/-/issues/217668#note_822456105
    # TODO: remove this when https://gitlab.com/gitlab-org/gitlab/-/issues/217668 has been completed.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        CI_COMMIT_BRANCH: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    # For merge requests, create a pipeline.
    - if: $CI_MERGE_REQUEST_IID
    # When a new tag is created, run a pipeline.
    - if: $CI_COMMIT_TAG
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, manually triggering a master pipeline, etc.).
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

shell check:
  image: koalaman/shellcheck-alpine:stable
  stage: pre-build
  before_script:
    - shellcheck --version
  script:
    - shellcheck build/gemnasium-maven/*/install.sh build/gemnasium-maven/*/config/.bashrc

shfmt:
  image: mvdan/shfmt:v3.1.0-alpine
  stage: pre-build
  before_script:
    - shfmt -version
  script:
    - shfmt -i 2 -ci -d build/gemnasium-maven/*/install.sh build/gemnasium-maven/*/config/.bashrc

.go:
  stage: pre-build

danger-review:
  stage: pre-build

npm vrange test:
  image: node:16-alpine3.16
  stage: pre-build
  before_script:
    - cd vrange/npm
  script:
    - yarn install
    - ./test_rangecheck.js

php vrange test:
  image: alpine:3.16
  stage: pre-build
  before_script:
    - cd vrange/php
  script:
    - apk add --no-cache git
    - apk add --no-cache --repository "https://dl-cdn.alpinelinux.org/alpine/v3.15/community" php7 php7-dom php7-ctype php7-tokenizer php7-xmlwriter php7-xml
    - apk add --no-cache --repository "https://dl-cdn.alpinelinux.org/alpine/v3.14/community" "composer=~2.1"
    - composer install
    - ./vendor/bin/phpunit --bootstrap vendor/autoload.php RangeCheckTest

gem vrange test:
  image: ruby:3.1-alpine3.16
  stage: pre-build
  before_script:
    - cd vrange/gem
  script:
    - bundle install
    - rake test

python vrange test:
  image: python:3.9-alpine3.16
  stage: pre-build
  before_script:
    - cd vrange/python
  script:
    - pip install -r requirements.txt
    - python -m unittest rangecheck_test.py

dotnet vrange test:
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  stage: pre-build
  before_script:
    - cd vrange/nuget/VrangeTest
  script:
    - dotnet test

dependency_scanning:
  before_script:
    - rm -rf qa/fixtures

sast:
  before_script:
    - rm -rf qa/fixtures

spotbugs-sast:
  before_script:
    - rm -rf qa/fixtures
    - rm -rf finder/testdata

secret_detection:
  variables:
    SECURE_LOG_LEVEL: "debug"
  before_script:
    - rm -rf qa/fixtures

sbomgen-golang:
  stage: image
  trigger:
    include: .gitlab/ci/sbomgen-golang.gitlab-ci.yml
    strategy: depend
    forward:
      pipeline_variables: true
  rules:
    - if: $CI_MERGE_REQUEST_TITLE =~ /\[sbomgen-golang only\]/
      when: on_success
    - if: $CI_MERGE_REQUEST_TITLE =~ /only\]/
      when: never
    - when: on_success

gemnasium:
  stage: image
  trigger:
    include: .gitlab/ci/gemnasium.gitlab-ci.yml
    strategy: depend
    forward:
      pipeline_variables: true
  rules:
    - if: $CI_MERGE_REQUEST_TITLE =~ /\[gemnasium only\]/
      when: on_success
    - if: $CI_MERGE_REQUEST_TITLE =~ /only\]/
      when: never
    - when: on_success

gemnasium-maven:
  stage: image
  trigger:
    include: .gitlab/ci/gemnasium-maven.gitlab-ci.yml
    strategy: depend
    forward:
      pipeline_variables: true
  rules:
    - if: $CI_MERGE_REQUEST_TITLE =~ /\[gemnasium-maven only\]/
      when: on_success
    - if: $CI_MERGE_REQUEST_TITLE =~ /only\]/
      when: never
    - when: on_success

gemnasium-python:
  stage: image
  trigger:
    include: .gitlab/ci/gemnasium-python.gitlab-ci.yml
    strategy: depend
    forward:
      pipeline_variables: true
  rules:
    - if: $CI_MERGE_REQUEST_TITLE =~ /\[gemnasium-python only\]/
      when: on_success
    - if: $CI_MERGE_REQUEST_TITLE =~ /only\]/
      when: never
    - when: on_success

# run container scanning in parent pipeline to collect security reports
# TODO: remove this job once reports can be read from child pipelines;
# see https://gitlab.com/gitlab-org/gitlab/-/issues/215725
container_scanning:
  variables:
    TMP_IMAGE_PATH: "/tmp/$IMAGE_ALIAS"
    DOCKER_IMAGE: "$CI_REGISTRY_IMAGE$TMP_IMAGE_PATH:$CI_COMMIT_SHA$IMAGE_TAG_SUFFIX"
  parallel:
    matrix:
      - IMAGE_ALIAS:
        - main
        - maven
        - python

container_scanning-fips:
  extends: container_scanning
  variables:
    IMAGE_TAG_SUFFIX: "-fips"

upsert git tag:
  stage: tag
